# My startpages

#### What are startpages?

Startpages are the home of your browser, they can contain useful information such as recently visited websites, time and weather data.

#### Where to find my startpages?

The code for a startpage can be found under __src/__ of any startpage folder, there is also an extension available for each startpage under __src/web-ext-artifacts/__

#### To-do:
- [x] startpage_3 is on its way: it will be just as minimal as previous startpages, but as with my other two startpages, each one contains a unique feature to make it stand out from the rest.

## How to use the startpages:
### Set it as home page:
First, let's clone the repo
Set it as a home page for Firefox:
1. Open Firefox and click on the hamburger menu on the top right
2. Click Preferences
3. Click Home
4. Next to "Homepage and new windows" is a drop down list: select _Custom URLs_, which we will use to specify the URL of the _startpage_1.html_ local file.
5. Paste the URL of _startpage.html_, this URL will look like: _file:///path/to/your/startpage.html_
